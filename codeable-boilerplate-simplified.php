<?php
/*
Plugin Name: Codeable Boilerplate Simplified
Description: This repo is an example for Codeable experts of how namespaces, dependency injection, DI container can be wired together. This is not production ready code, for a proper framework check https://compose.press/
Author: Derrick Hammer / Václav Greif
Version: 1.0
*/

namespace Codeable\BoilerplateSimplified;


require_once 'vendor/autoload.php';

/**
 * Singleton instance function. We will not use a global at all as that defeats the purpose of a singleton and is a bad design overall
 * @return Plugin
 */
function codeable_boilerplate() {
	return codeable_boilerplate_container()->create( '\Codeable\BoilerplateSimplified\Plugin' );
}

/**
 * Container singleton
 * @return Dice
 */
function codeable_boilerplate_container() {
	static $container;
	if ( empty( $container ) ) {
		$container = ( new Dice() )->addRules( codeable_boilerplate_container_rules() );
	}

	return $container;
}

/**
 * Get the DI container rules
 * @return array
 */
function codeable_boilerplate_container_rules() {
	return [
		'Codeable\BoilerplateSimplified\Plugin' => [
			'shared'        => true,
			'substitutions' => [
				'Codeable\BoilerplateSimplified\Interfaces\MyComponentInterface' => 'Codeable\BoilerplateSimplified\MyComponent',
			],
		],
	];
}

codeable_boilerplate();