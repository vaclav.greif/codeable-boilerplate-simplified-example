<?php


namespace Codeable\BoilerplateSimplified;


class ThirdComponent {
	/**
	 * @var int
	 */
	private $id;

	public function __construct( int $id ) {
		$this->id = $id;
	}

	/**
	 * Initialize hooks
	 */
	public function init() {
		add_action( 'init', [ $this, 'do_something' ] );
	}

	/**
	 * Do something
	 */
	public function do_something() {
		echo 'I did something';
	}
}