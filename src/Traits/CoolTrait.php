<?php

namespace Codeable\BoilerplateSimplified\Traits;

trait CoolTrait {
	function cool_functionality() {
		return "I'm so cool";
	}
}