<?php

namespace Codeable\BoilerplateSimplified;

use Codeable\BoilerplateSimplified\Interfaces\MyComponentInterface;

/**
 * Class Plugin
 * @package Codeable\BoilerplateSimplified
 */
class Plugin {
	/**
	 * @var MyComponent
	 */
	private $my_component;
	/**
	 * @var AnotherComponent
	 */
	private $another_component;

	/**
	 * Plugin constructor.
	 *
	 * @param MyComponentInterface $my_component
	 * @param AnotherComponent $another_component
	 */
	public function __construct( MyComponentInterface $my_component, AnotherComponent $another_component ) {
		$this->my_component      = $my_component;
		$this->another_component = $another_component;
	}

	/**
	 * @return MyComponentInterface|MyComponent
	 */
	public function get_my_component() {
		return $this->my_component;
	}

	/**
	 * @return AnotherComponent
	 */
	public function get_another_component() {
		return $this->another_component;
	}

	/**
	 * @return LazyLoadedComponent
	 */
	public function get_lazy_loaded_component() {
		return codeable_boilerplate_container()->create( '\Codeable\BoilerplateSimplified\LazyLoadedComponent' );
	}

	/**
	 * @param int $id
	 *
	 * @return LazyLoadedComponentWithArgs
	 */
	public function get_lazy_loaded_component_with_args( int $id ) {
		return codeable_boilerplate_container()->create( '\Codeable\BoilerplateSimplified\LazyLoadedComponent', [ $id ] );
	}

	/**
	 * @param int $id
	 *
	 * @return object
	 */
	public function get_third_component( int $id ) {
		try {
			return $this->load_component( '\Codeable\BoilerplateSimplified', [ $id ] );
		} catch ( \Exception $e ) {
		}
	}

	/**
	 * @param string $class_name
	 * @param array $args
	 *
	 * @return object
	 * @throws \Exception
	 */
	public function load_component( $class_name, $args = [] ) {
		if ( ! class_exists( $class_name ) ) {
			throw new \Exception( 'Class does not exist.' );
		}

		$component = codeable_boilerplate_container()->create( $class_name, $args );
		if ( method_exists( $component, 'init' ) ) {
			$component->init();
		}

		return $component;
	}
}
