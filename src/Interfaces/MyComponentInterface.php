<?php
namespace Codeable\BoilerplateSimplified\Interfaces;

interface MyComponentInterface {
	public function get_value() : string ;
}