<?php

namespace Codeable\BoilerplateSimplified;

use Codeable\BoilerplateSimplified\Interfaces\MyComponentInterface;

/**
 * Class MyComponent
 * @package Codeable\BoilerplateSimplified
 */
class MyComponent implements MyComponentInterface {

	public function get_value(): string {
		return 'Hello';
	}
}