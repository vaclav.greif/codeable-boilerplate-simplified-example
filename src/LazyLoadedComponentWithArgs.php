<?php


namespace Codeable\BoilerplateSimplified;


class LazyLoadedComponentWithArgs {
	/**
	 * @var int
	 */
	private $id;

	public function __construct(int $id) {
		$this->id = $id;
	}
}