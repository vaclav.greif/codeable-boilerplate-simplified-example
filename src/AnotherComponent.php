<?php

namespace Codeable\BoilerplateSimplified;

use Codeable\BoilerplateSimplified\Traits\CoolTrait;

class AnotherComponent {
	use CoolTrait;

	public function setup() {
		// Do something
	}
}